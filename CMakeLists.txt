cmake_minimum_required(VERSION 3.6)
project(otf2_communication_matrix)

set(CMAKE_C++_STANDARD 17)


# CMakeFind repository
list (APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake_modules)

#Include the directories and now your cpp files will recognize your headers
find_package (OTF2 REQUIRED)
if (NOT OTF2_FOUND)
   message(FATAL_ERROR "libotf2 not found.")
endif()

add_subdirectory (src)
