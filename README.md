# OTF2 Communication Matrix

We provide a program to generate a communication matrix for a MPI program OTF2 trace.



## Building OTF2 Communication matrix

You just need to compile using CMake:

```
cd otf2-communication-matrix
mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$INSTALLATION_DIR
make && make install
```

This will create the otf2_comm_matrix program.

## Usage

First, generate an OTF2 trace with your favorite tracing tool. We recommend using EZTrace:

```
$ mpirun -np 4 eztrace -t mpi ./my_application
[...]

```
### OTF2 Communication Matrix
Then use `otf2_comm_matrix <your program>_trace/eztrace_log.otf2 > matrix.mat` to generate a communication matrix.

This will generate a `matrix.mat` file containing the matrix under csv format.

The full option list of the `otf2_comm_matrix` are as follows :

```
-? -h: Display help and full description of the program
```

### Plotting Communication Matrix

We also provide a ploting script `plot_comm_matrix.py` using Python
Matplotlib (dependencies are numpy and Matplotlib). Simply give in
parameter any path to a `.mat` file generated by the
`otf2_comm_matrix` program.


