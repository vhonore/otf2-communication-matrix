import matplotlib.pyplot as plt
import numpy as np
import sys


# Check the path to the matrix is given in parameters
if (len(sys.argv) !=2):
    print("[ERROR] Expecting exactly one argument : the path to the .mat file that contains the matrix to plot.\n")
    exit(-1)

# Set file name
file = sys.argv[1]

# Load matrix
mat = np.loadtxt(open(file, "rb"), delimiter=",", skiprows=1)

# Normalize to MegaBytes
mat = mat/1000000

# Create figure
fig = plt.figure()
ax = fig.add_subplot(111)

# Show the matrix
cax = ax.matshow(mat, interpolation='nearest', origin='lower')
# Show the colorbar
fig.colorbar(cax,label="Size (MB)")
plt.gca().xaxis.tick_bottom()

# Formatting
plt.title("MPI Communication Matrix",fontsize=18)
plt.xlabel("Sender Rank",fontsize=16)
plt.ylabel("Receiver Rank",fontsize=16)

# Save figure
plt.savefig("plot_comm_matrix.pdf")


